const names = ["Adri", "Alex", "Anne-Cha", "Cécile", "Diane", "JL", "Mélo", "Thierry"];

function getNames() {
    return names;
}

const recipients = [6, 0, 7, 1, 3, 2, 4, 5];

function getRecipient(selectedName) {
    const nameIndex = names.findIndex(name => name === selectedName);
    const recipientIndex = recipients[nameIndex];
    return names[recipientIndex];
}